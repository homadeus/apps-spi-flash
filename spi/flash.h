#ifndef __SPI_FLASH_H__
#define __SPI_FLASH_H__

#include <stdint.h>
#include <stdlib.h>

#include <spi/spi.h>

#define FLASH_COMMANDS \
   X(WRITE_STATUS,              0x01) \
   X(PAGE_PROGRAM,              0x02) \
   X(READ_ARRAY_SLOW,           0x03) \
   X(WRITE_DISABLE,             0x04) \
   X(READ_STATUS,               0x05) \
   X(WRITE_ENABLE,              0x06) \
   X(FAST_READ,                 0x0b) \
   X(READ_SERIAL,               0x4b) \
   X(ERASE_4K,                  0x20) \
   X(ERASE_32K,                 0x52) \
   X(ERASE_CHIP,                0xc7) \
   X(ERASE_64K,                 0xd8) \
   X(READ_ID,                   0x9f)

#define STATUS_BUSY				0x01

#define MAX_ATTEMPTS_WRITE		(F_CPU) * 100
#define MAX_ATTEMPTS_ERASE		(F_CPU) * 100

typedef enum {
#define X(a, b) CMD_##a = b,
   FLASH_COMMANDS
#undef X
} flash_commands_t;

struct spi_flash {
	struct spi_slave *spi;
	const char	     *name;
	uint32_t		 size;
	uint32_t		 page_size;
	uint32_t		 sector_size;
};

int spi_flash_probe(struct spi_flash *flash, struct spi_slave *spi);

int spi_flash_read(struct spi_flash *flash, uint32_t offset,
		size_t len, void *buf);

int spi_flash_read_serial(struct spi_flash *flash, uint8_t *data);

int spi_flash_write(struct spi_flash *flash, uint32_t offset,
		size_t len, const void *buf);

int spi_flash_erase(struct spi_flash *flash, uint32_t offset,
		size_t len);

#endif /* end of include guard: __SPI_FLASH_H__ */
