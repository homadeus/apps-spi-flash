#include <spi/flash.h>
#include <spi/spi.h>
#include <homadeus/utils/utils.h>
#include <homadeus/utils/debug.h>

struct supported_spi_chips {
	uint8_t manufacturer_id;
	uint16_t id;
	uint32_t page_size;
	uint32_t sector_size;
	uint16_t nr_sector;
	char* name;
};

static const struct supported_spi_chips supported_spi_chips_table[] = {
	{ 0xEF, 0x4015, 256, 4096, 512, "W25Q16" },
	{ 0xEF, 0x6015, 256, 4096, 512, "W25Q16" },
};

static void flash_cmd_addr(uint8_t *cmd, uint8_t command, uint32_t addr)
{
    cmd[0] = command;
	cmd[1] = addr >> 16;
	cmd[2] = addr >> 8;
	cmd[3] = addr >> 0;
}

static int flash_read_write(struct spi_slave *spi,
				const uint8_t *cmd, size_t cmd_len,
				const uint8_t *data_out, uint8_t *data_in,
				size_t data_len)
{
	unsigned long flags = SPI_XFER_BEGIN;
	int ret;

	if (data_len == 0)
		flags |= SPI_XFER_END;

	ret = spi_xfer(spi, cmd_len * 8, cmd, NULL, flags);
	if (ret) {
		debug("SF: Failed to send command (%zu bytes): %d\n",
				cmd_len, ret);
	} else if (data_len != 0) {
		ret = spi_xfer(spi, data_len * 8, data_out, data_in, SPI_XFER_END);
		if (ret) {
			debug("SF: Failed to transfer %zu bytes of data: %d\n",
					data_len, ret);
		}
	}

	return ret;
}

static int flash_cmd_read(struct spi_slave *spi, const uint8_t *cmd,
		size_t cmd_len, void *data, size_t data_len)
{
	return flash_read_write(spi, cmd, cmd_len, NULL, data, data_len);
}

static int flash_cmd(struct spi_slave *spi, uint8_t cmd, void *response, size_t response_len)
{
	return flash_cmd_read(spi, &cmd, 1, response, response_len);
}

static int flash_cmd_write(struct spi_slave *spi, const uint8_t *cmd, size_t cmd_len,
		const void *data, size_t data_len)
{
	return flash_read_write(spi, cmd, cmd_len, data, NULL, data_len);
}

static inline int flash_write_enable(struct spi_flash *flash, uint8_t enabled)
{
	if (enabled)
		return flash_cmd(flash->spi, CMD_WRITE_ENABLE, NULL, 0);
	else
		return flash_cmd(flash->spi, CMD_WRITE_DISABLE, NULL, 0);
}

int flash_poll_status(struct spi_flash *flash, unsigned long attempts,
			   uint8_t cmd, uint8_t status_mask)
{
	struct spi_slave *spi = flash->spi;
	int ret = spi_xfer(spi, 8, &cmd, NULL, SPI_XFER_BEGIN);

	uint8_t status = 0;

	if (ret)
		return ret;

	do {
		ret = spi_xfer(spi, 8, NULL, &status, 0);
		if (ret)
			return ret;

		if (!(status & status_mask))
			break;

	} while (--attempts);

	spi_xfer(spi, 0, NULL, NULL, SPI_XFER_END);

	if (!(status & status_mask))
		return 0;

	return 1;
}

int spi_flash_read_serial(struct spi_flash *flash, uint8_t *data)
{
	uint8_t cmd[5] = { CMD_READ_SERIAL, 0, 0, 0, 0 };

#if defined(SPI_HAVE_CLAIM_BUS)
	spi_claim_bus(flash->spi);
#endif
	int ret = flash_cmd_read(flash->spi, cmd, sizeof(cmd), data, 8);
#if defined(SPI_HAVE_CLAIM_BUS)
	spi_release_bus(flash->spi);
#endif

	return ret;
}

int spi_flash_erase(struct spi_flash *flash, uint32_t offset, size_t len)
{
	uint8_t cmd_erase;
	uint8_t cmd[4];
	uint32_t start, end, erase_size;

	int ret;

	erase_size = flash->sector_size;

	if (offset % erase_size || len % erase_size)
		return 1;

#if defined(SPI_HAVE_CLAIM_BUS)
	ret = spi_claim_bus(flash->spi);
	if (ret)
		return ret;
#endif

	if (erase_size == 4096)
		cmd_erase = CMD_ERASE_4K;
	else if (erase_size == 65536)
		cmd_erase = CMD_ERASE_64K;
	else
		return 1;

	start = offset;
	end = start + len;

	while (offset < end) {
		flash_cmd_addr(cmd, cmd_erase, offset);
		offset += erase_size;

		ret = flash_write_enable(flash, 1);
		if (ret)
			goto out;

		ret = flash_cmd_write(flash->spi, cmd, sizeof(cmd), NULL, 0);
		if (ret)
			goto out;

		ret = flash_poll_status(flash, MAX_ATTEMPTS_ERASE, CMD_READ_STATUS,
				STATUS_BUSY);

		if (ret)
			goto out;
	}

	ret = flash_write_enable(flash, 0);

 out:
#if defined(SPI_HAVE_CLAIM_BUS)
	spi_release_bus(flash->spi);
#endif
	return ret;
}

int spi_flash_write(struct spi_flash *flash, uint32_t offset,
		size_t len, const void *buf)
{
	unsigned long page_addr, byte_addr, page_size;
	size_t chunk_len, actual;
	int ret;
	uint8_t cmd[4];

	page_size = flash->page_size;
	page_addr = offset / page_size;
	byte_addr = offset % page_size;

#if defined(SPI_HAVE_CLAIM_BUS)
	ret = spi_claim_bus(flash->spi);
	if (ret) {
		return ret;
	}
#endif

	cmd[0] = CMD_PAGE_PROGRAM;
	for (actual = 0; actual < len; actual += chunk_len) {
		chunk_len = min(len - actual, page_size - byte_addr);

		cmd[1] = page_addr >> 8;
		cmd[2] = page_addr;
		cmd[3] = byte_addr;


		ret = flash_write_enable(flash, 1);
		if (ret)
			break;

		ret = flash_cmd_write(flash->spi, cmd, 4,
					  buf + actual, chunk_len);
		if (ret < 0)
			break;

		ret = flash_poll_status(flash, MAX_ATTEMPTS_WRITE, CMD_READ_STATUS,
				STATUS_BUSY);
		if (ret)
			break;

		page_addr++;
		byte_addr = 0;
	}

	ret = flash_write_enable(flash, 0);

#if defined(SPI_HAVE_CLAIM_BUS)
	spi_release_bus(flash->spi);
#endif
	return ret;
}
int spi_flash_read(struct spi_flash *flash, uint32_t offset,
		size_t data_len, void *data)
{
	struct spi_slave *spi = flash->spi;

	uint8_t cmd[5];
	flash_cmd_addr(cmd, CMD_FAST_READ, offset);
	cmd[4] = 0x00;

#if defined(SPI_HAVE_CLAIM_BUS)
	spi_claim_bus(spi);
#endif
	int ret = flash_cmd_read(spi, cmd, sizeof(cmd), data, data_len);
#if defined(SPI_HAVE_CLAIM_BUS)
	spi_release_bus(spi);
#endif

	return ret;
}

int spi_flash_probe(struct spi_flash *flash, struct spi_slave *spi)
{
	int rc = 1, ret;

#if defined(SPI_HAVE_CLAIM_BUS)
	ret = spi_claim_bus(spi);
	if (ret)
		goto err_claim_bus;
#endif

	uint8_t idcode[5];
	ret = flash_cmd(spi, CMD_READ_ID, idcode, sizeof(idcode));
	if (ret)
	    goto err_read;

	for (unsigned int i = 0; i < ARRAY_SIZE(supported_spi_chips_table); i++) {
		if (supported_spi_chips_table[i].manufacturer_id == idcode[0]) {
			if (supported_spi_chips_table[i].id == (uint16_t) ((idcode[1] << 8) | idcode[2])) {
				flash->spi = spi;
				flash->page_size = supported_spi_chips_table[i].page_size;
				flash->sector_size = supported_spi_chips_table[i].sector_size;
				flash->size = supported_spi_chips_table[i].sector_size * supported_spi_chips_table[i].nr_sector;
				flash->name = supported_spi_chips_table[i].name;
				rc = 0;
			}
		}

	}

err_read:
#if defined(SPI_HAVE_CLAIM_BUS)
    spi_release_bus(spi);
err_claim_bus:
#endif
	return rc;
}
